# Auto-reload HTML as described in https://stackoverflow.com/a/58745863

GITHUB_STYLESHEET ?= $(HOME)/src/markdown-css/github.css
PANDOC_OPTS :=  --css=$(GITHUB_STYLESHEET) --highlight-style=haddock --to=html5 --self-contained --metadata pagetitle=ignore

.PHONY : render
render : README.html
%.html : %.md
	while true; do pandoc $(PANDOC_OPTS) -i $< -o $@ ; inotifywait -e modify $< ; done
