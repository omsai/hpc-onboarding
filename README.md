# HPC onboarding

Practice and feedback and essential for learning.
Using interactive grading,
these self-guided exercises will help you learn the essential HPC skills of
working on the command line,
working with the job scheduler, and
troubleshooting common problems.

## Learning goals

1. Basic fluency with command line: navigation, text editor, job scheduler, and
   common utilities.
1. Submit jobs to the scheduler.
1. Run interactive jobs.
1. Transfer files between local files, fast cluster filesystems and slow
   archival filesystems.
1. Maximize job performance.
1. Troubleshoot typical problems.

## Learning objectives

1. Navigate the UNIX filesystem, understand the difference between absolute and
   relative file paths, and use these commands effectively:

        ssh
		ls cd mkdir
		less nano
		sbatch srun sacct squeue sstat scancel
		tail watch htop
		wget tar find grep
1. Create, submit, inspect, and modify scheduler jobs.
1. Synchronize files using Globus.
1. Correctly use scheduler parallelism options of `--cpus-per-task`, and
   `--ntasks` for CPUs, or `--gres` for GPUs.  Use GNU parallel or MPI for
   large scale parallelism and checkpointing.  Understand available queues,
   inspect and optimize queuing time using `--partition` and `--time`.
1. Fix typical typos, fix typical errors from utlity commands, troubleshoot
   submission failures, and troubleshoot runtime failures of programatic
   failures, obfuscated MPI errors, and running out memory.

## Assessment requirements

1. Students should get instant, constructive feedback of their submissions.
1. Submissions should be logged to improve questions, feedback, and estimate
   typical completion time.
1. Students should not be able to directly see the solutions, but have a score
   for how correct their solution is, see a `diff` comparing their output with
   the expected output, etc.

## Assessment implementation

1. Create server program so that it's filesystem containing solutions is
   inaccessible to the students.  Run submission scripts in some sort of chroot
   jail like sandbox to guard against commands like `ls`, `cat`, etc. then diff
   the output against solutions.  But the sandbox still needs to communicate
   with the SLURM scheduler.
1. The server program will also log all student submission attempts.
1. All questions should be versioned for comparing changes to questions.
